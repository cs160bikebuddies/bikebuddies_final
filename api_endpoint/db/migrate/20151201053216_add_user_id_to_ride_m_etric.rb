class AddUserIdToRideMEtric < ActiveRecord::Migration
	def up
		add_column :ride_metrics, :ride_id, :integer
		add_column :ride_metrics, :user_id, :integer
	end

	def down
  		remove_column :ride_metrics, :ride_id, :integer
  		remove_column :ride_metrics, :user_id, :integer
  	end
end
