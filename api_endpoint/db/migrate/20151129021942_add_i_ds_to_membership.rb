class AddIDsToMembership < ActiveRecord::Migration
  def up
  	add_column :memberships, :group_id, :integer
  	add_column :memberships, :user_id, :integer
  end

  def down
  	remove_column :memberships, :group_id, :integer
  	remove_column :memberships, :user_id, :integer
  end
end
