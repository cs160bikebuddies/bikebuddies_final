class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.string :ride_name
      t.datetime :ride_start_time
      t.string :start_location
      t.string :dest_location
      t.float :length_miles

      t.timestamps
    end
  end
end
