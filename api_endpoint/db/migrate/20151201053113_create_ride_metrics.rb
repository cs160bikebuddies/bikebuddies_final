class CreateRideMetrics < ActiveRecord::Migration
  def change
    create_table :ride_metrics do |t|
      t.float :avg_speed
      t.float :bpm

      t.timestamps
    end
  end
end
