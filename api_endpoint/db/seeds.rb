# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
users = User.create([	{name: 'Vy', username: "vy", password: "123456", experience: "Beginner",
							profile_pic_url: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/supportfemale-512.png"},
						{name: 'Shreya', username: "shreya", password: "pass", experience: "Intermediate",
							profile_pic_url: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/girl-512.png"},
						{name: 'Matt', username: "matt", password: "654321", experience: "Advanced",
							profile_pic_url: "https://www.staffsprep.com/software/flat_faces_icons/png/flat_faces_icons_circle/flat-faces-icons-circle-6.png"},
						{name: 'Chris', username: "chris", password: "132435", experience: "Intermediate",
							profile_pic_url: "https://cdn0.iconfinder.com/data/icons/user-pictures/100/matureman1-512.png",},
						{name: 'Kevin', username: "kevin", password: "password", experience: "Beginner",
							profile_pic_url: "https://cdn1.iconfinder.com/data/icons/user-pictures/101/malecostume-512.png"},
						{name: 'Jeff', username: "jeff", password: "battery", experience: "Beginner",
							profile_pic_url: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/supportmale-512.png"},
						{name: 'Jill', username: "jill", password: "staple", experience: "Intermediate",
							profile_pic_url: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/girl-512.png"},
						{name: 'Sarah', username: "sarah", password: "horse", experience: "Advanced",
							profile_pic_url: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/female1-512.png"}])

groups = Group.create([	{name: 'Casual Berkeley Bikers', location: "Berkeley, CA", experience: "Beginner", description: "A group of amatuer bikers based in Berkeley"},
						{name: 'Bay Area Friends', experience: "Intermediate", location: "Oakland, CA", description: "We're a group of cylists based in the Bay Area! Come join us."},
						{name: 'SF Cycling Club', experience: "Beginner", location: "San Francisco, CA", description: "SF Cycling Club for people looking to get into biking"},
						{name: 'Berkeley Competitive Biking Team',experience: "Advanced", location: "Berkeley, CA", description: "We are the competitive biking team from UC Berkeley"},
						{name: 'Palo Alto Bikers', experience: "Beginner", location: "Palo Alto, CA", description: "Group of friends from Palo Alto"},
						{name: 'Berkeley Bike Training Group', experience: "Intermediate", location: "Berkeley, CA", description: "Bikers who want to get more serious about biking"}])

rides = Ride.create([	{ride_name: "Oakland To Berkeley", ride_start_time: Time.new(2015, 12, 8, 14, 30, 0),
							length_miles: 5.7, start_location: "12th St. Oakland City Center, Oakland, CA", dest_location: "2556 Haste St, Berkeley, CA 94704"},
						{ride_name: "Marina Ride", ride_start_time: Time.new(2015, 12, 10, 12, 45, 0),
							length_miles: 3.2, start_location: "2407 Dana St, Berkeley, CA 94704",  dest_location: "Berkeley Marina, Berkeley, CA"},
						{ride_name: "Competition Training", ride_start_time: Time.new(2015, 12, 1, 10, 30, 0),
							length_miles: 16.9, start_location: "100 Hamilton Ave, Palo Alto, CA 94301",  dest_location: "525 W Santa Clara St, San Jose, CA 95113"},
						{ride_name: "SF Loop", ride_start_time: Time.new(2015, 12, 17, 8, 30, 0),
							length_miles: 3.0, start_location: "Mission District, San Francisco, CA", dest_location: "501 Twin Peaks Blvd, San Francisco, CA 94114"},
						{ride_name: "Berkeley Training", ride_start_time: Time.new(2015, 12, 2, 15, 00, 0),
							length_miles: 3.2, start_location: "UC Berkeley, CA",  dest_location: "Pacific East Mall, 3288 Pierce St #99, Richmond, CA 94804"},
						{ride_name: "Long Distance Training", ride_start_time: Time.new(2015, 12, 12, 9, 30, 0),
							length_miles: 9.3, start_location: "University of California, Berkeley, Berkeley, CA", dest_location: "2317 Central Ave, Alameda, CA 94501"}] )

groups[0].users << users[0]
groups[1].users << users[1]
groups[2].users << users[2]
groups[0].users << users[3]
groups[4].users << users[4]
groups[5].users << users[5]
groups[0].users << users[6]
groups[1].users << users[7]
groups[2].users << users[3]
groups[0].users << users[4]
groups[5].users << users[1]
groups[0].users << users[2]
groups[2].users << users[7]
groups[3].users << users[6]
groups[0].users << users[5]
groups[3].users << users[4]
groups[5].users << users[3]
groups[3].users << users[2]
groups[0].users << users[1]
groups[2].users << users[0]
groups[2].users << users[1]
groups[1].users << users[0]

def add_ride_to_group(x, y, groups, rides)
	groups[x].rides << rides[y]
	groups[x].users.each do |user|
		if !user.rides.include?(rides[y])
			rides[y].users << user
		end
	end
end


add_ride_to_group(0, 0, groups, rides)
add_ride_to_group(0, 1, groups, rides)
add_ride_to_group(0, 4, groups, rides)

add_ride_to_group(1, 1, groups, rides)

add_ride_to_group(2, 4, groups, rides)
add_ride_to_group(2, 0, groups, rides)

add_ride_to_group(3, 4, groups, rides)
add_ride_to_group(3, 5, groups, rides)
add_ride_to_group(3, 0, groups, rides)

add_ride_to_group(4, 2, groups, rides)

add_ride_to_group(5, 1, groups, rides)
add_ride_to_group(5, 5, groups, rides)
