class Api::V1::UsersController < ApplicationController
	skip_before_filter :verify_authenticity_token

	#GET /api/v1/users
	def index
		@user = User.all
		render json: @user
	end

	#GET /api/v1/users/:username/:password/valid
	def validate
		print params[:username]
		@user = User.find_by_username(params[:username])
		render json: {valid: @user.password == params[:password] ? true : false, user: @user}
	end

	#POST /api/v1/users
	def create
		@user = User.new(create_params)
		@user.save
		render json: @user
	end

	#GET /api/v1/users/:id/edit
	def edit
		render json: {status: "Success"}
	end

	#PUT /api/v1/users/:id/edit
	def update
		@user = User.find(params[:id])
		@user.update!(update_params)
		render json: @user
	end

	#DELETE /api/v1/users/:id
	def destroy
		@user = User.find(params[:id])
		@user.destroy!
		render json: @user
	end

	#GET /api/v1/users/:id
	def show
		@user = User.find(params[:id])
		@groups = @user.groups
		render json: {user: @user, groups: @groups}
	end

	#POST /api/v1/users/:id/groups/:group_id
	def add_to_group
		@user = User.find(params[:id])
		@groups = Group.find(params[:group_id])
		@groups.users << @user
		render json: {user: @user, groups: @groups}
	end

	#GET /api/v1/users/:id/rides
	def rides
		@user = User.find(params[:id])
		@rides = @user.rides.select { |ride| ride.ride_start_time > Time.now }.sort_by {|ride| ride.ride_start_time}
		render json: @rides
	end

	#GET /api/v1/users/:id/ride_history
	def ride_history
		@user = User.find(params[:id])
		@rides = @user.rides
		render json: {rides: @rides, statistics: {total_length: @rides.reduce(0){|x,y| x + y.length_miles}, num_rides:
		@rides.size, avg_ride_lengh: @rides.reduce(0){|x,y| x + y.length_miles} / @rides.size.to_f}}
	end

	private
	def create_params
		params.require(:user).permit(:name, :username, :password, :experience, :profile_pic_url)
  	end

  	def update_params
		params.require(:user).permit(:name, :username, :password, :experience, :profile_pic_url)
  	end
end
