class Api::V1::GroupsController < ApplicationController
	skip_before_filter :verify_authenticity_token
	#GET /api/v1/groups/:level/filter
	def group_level
		@group = Group.select{|group| group.experience == params[:level]}
		render json: @group
	end

	#GET /api/v1/groups/:level/filter/:user_id
	def group_level_user_filter
		@user = User.find(params[:user_id])
		@group = Group.select{|group| group.experience == params[:level]}.select{|group| !group.users.include?(@user)}
		render json: @group
	end

	#GET /api/v1/groups
	def index
		@group = Group.all
		render json: @group
	end

	#POST /api/v1/groups
	def create
		print create_params
		@group = Group.new(create_params)
		@group.save!
		render json: @group
	end

	#GET /api/v1/groups/:id
	def show
		@group = Group.find(params[:id])
		render json: @group
	end

	#PUT /api/v1/groups/:id/
	def update
		@group = Group.find(params[:id])
		@group.update!(update_params)
		render json: @group
	end

	#GET /api/v1/groups/:id/users
	def users
		@group = Group.find(params[:id])
		render json: @group.users
	end

	#POST /api/v1/groups/:id/ride/:user_id
	def create_ride
		@group = Group.find(params[:id])
		@ride = Ride.new(ride_params)
		@group.rides << @ride
		@ride.user = User.find(params[:user_id])
		@group.users.each do |user|
			if !user.rides.include?(@ride)
				@ride.users << user
			end
		end
		@ride.save!
		render json: @ride
	end


	private
	def create_params
		params.require(:group).permit(:name, :experience, :location, :description)
  	end

  	def ride_params
  		@ride_params ||= params.require(:ride).permit(:ride_start_time, :start_location, :dest_location, :length_miles, :ride_name)
  		@ride_params[:ride_start_time] = params[:ride][:ride_start_time].to_datetime
  		@ride_params
  	end

  	def update_params
		params.require(:group).permit(:name, :experience, :location, :description)
  	end
end
