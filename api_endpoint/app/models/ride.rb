class Ride < ActiveRecord::Base
	belongs_to :group
	belongs_to :user
	has_many :ride_metrics
	has_many :users, through: :ride_metrics

	def ride_creator
		self.user
	end
end
