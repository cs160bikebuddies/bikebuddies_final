# README #

## CS160 Fall 2015 | Group 37: OG WatchMen | BikeBuddies ##

### What is this repository for? ###

* This repository contains the code for the server that handles the data in the backend, including the users and the groups.  The code for both the Android phone and watch apps is in the cs160bikebuddies/skeleton_code repository.

### How do I get set up? ###

Setup instructions will be added soon.

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions