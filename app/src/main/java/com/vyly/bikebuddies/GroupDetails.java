package com.vyly.bikebuddies;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GroupDetails extends AppCompatActivity implements View.OnClickListener {
    private String TAG = "GroupDetails";
    private ArrayList<String> member_pics;
    private ArrayList<String> member_names;
    private ArrayList<Integer> member_ids;
    private String group_id;
    private boolean done = false;
    private int user_id;
    TextView t;
    User me;

//    http://stackoverflow.com/questions/18936556/how-to-add-a-caption-to-a-gridview-cell

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);
        t = (TextView) findViewById(R.id.num_members);
        Intent i = getIntent();

        final Group groupme = (Group)i.getSerializableExtra("group");

        if(!groupme.getDescription().equals("null")){
            TextView t2 = (TextView) findViewById(R.id.fillDescription);
            t2.setText(groupme.getDescription());
        }


        member_pics = new ArrayList<>();
        member_names = new ArrayList<>();
        member_ids = new ArrayList<>();
        me = ((GlobalApplication) this.getApplication()).getUser();
        user_id = me.user_id;
        group_id =  groupme.getGroupId();
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setTitle(groupme.getGroupName());
        View addButton = findViewById(R.id.add_button);
        addButton.setOutlineProvider(new ViewOutlineProvider() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void getOutline(View view, Outline outline) {
                int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                outline.setOval(0, 0, diameter, diameter);
            }
        });
        addButton.setClipToOutline(true);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GroupDetails.this, RideCreate.class);
                i.putExtra("group_name", groupme.getGroupName());
                startActivity(i);
            }
        });

        final Button join = (Button)findViewById(R.id.joinGroupButton);
        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onclick");
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            addToGroup();

//                  }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
                update();
                join.setVisibility(View.INVISIBLE);

            }
        });

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    getGroupMembers(group_id);
//                  }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

        while(!this.done){

        }
        if(member_ids.contains(user_id)){
            join.setVisibility(View.INVISIBLE);
        }
        //TODO make this less janky
        if(this.done){
            t.setText(member_names.size() + " members");
            GridView g = (GridView) findViewById(R.id.grid_view);
            CustomAdapter adapter = new CustomAdapter(this, member_names, member_pics,member_ids);

            g.setAdapter(adapter);
        }

//        ((GlobalApplication) this.getApplication()).setSomeVariable("foo");
//        String s = ((GlobalApplication) this.getApplication()).getSomeVariable();
//        t.setText(s);
//        Log.i(TAG, s);

    }

    private void update(){
        member_names.add(me.name);
        member_ids.add(me.user_id);
        member_pics.add(me.profile_pic_url);

        t.setText(member_names.size() + " members");
        GridView g = (GridView) findViewById(R.id.grid_view);
        CustomAdapter adapter = new CustomAdapter(this, member_names, member_pics,member_ids);
        g.setAdapter(adapter);
    }

    private void getGroupMembers(String group_id){
        Log.i(TAG,"get group members");
        String request = getString(R.string.API_STEM) +"/groups/" + group_id + "/users";
        String result = Utilities.openURL(request);
        try {
            JSONArray resultObj = new JSONArray(result);

            int numMembers = resultObj.length() > 10 ? 10 : resultObj.length();
            for(int i=0; i < numMembers; i++) {
                JSONObject member = resultObj.getJSONObject(i);
                String member_name = member.getString("name");
                int id = (int)member.get("id");
                String profile_pic_url = member.getString("profile_pic_url");
                member_pics.add(profile_pic_url);
                member_names.add(member_name);
                member_ids.add(id);

            }
            this.done = true;
        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
    }

    public static void goToUserProfile(Context c,int user_id){
        Intent home = new Intent(c,UserProfile.class);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id",user_id);
        c.startActivity(home);

    }
    @Override
    public void onClick(View v) {
        Log.i(TAG, "onclick");
//        Intent myIntent = new Intent(GroupDetails.this,GroupJoin.class);
//        startActivity(myIntent);
        addToGroup();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addToGroup(){
        try{
            String create = getString(R.string.API_STEM) + "/users/" + user_id + "/groups/" + group_id ;
            URL url = new URL(create);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();

            int responseCode = conn.getResponseCode();
            Log.i(TAG,responseCode+" response");
            conn.disconnect();
        }catch(Exception e){
            Log.i(TAG,e.toString());
        }
    }

}
