package com.vyly.bikebuddies;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vyly on 11/29/15.
 */
@SuppressWarnings("serial") // hide compiler warning
public class Ride implements Serializable{
    private int ride_id;
    private String ride_name;
//    private String datetime;
    private String startLocation;
    private String endLocation;
    private String group_name;
    public static String API_ENDPOINT = "api/v1/ride";
    private Date datetime;

//    public Ride(int ride_id, String ride_name,Calendar datetime, String startLocation, String endLocation,String group_name){
//        this.ride_id = ride_id;
//        this.ride_name= ride_name;
//        this.datetime = datetime;
//        this.startLocation = startLocation;
//        this.endLocation = endLocation;
////        if(len(group_name){
//            this.group_name = group_name;
////        }
////        else{
////            this.group_name = "";
////
////        }
//    }
//    public Ride(int ride_id, String ride_name,String datetime,String group_name){
//        this.ride_id = ride_id;
//        this.ride_name = ride_name;
//        this.datetime = datetime;
//        this.group_name = group_name;
////        Log.i("new ride", group_name + ride_name);
//
//    }
    public Ride(int ride_id, String ride_name,Date datetime,String group_name,String startLocation, String endLocation){
        this.ride_id = ride_id; // this should be automatic?
        this.ride_name = ride_name;
        this.datetime = datetime;
        this.group_name = group_name;
        this.startLocation = startLocation;
        this.endLocation = endLocation;
//        Log.i("new ride", group_name + ride_name);

    }
    public String getRideDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("E MMM dd");
        return  dateFormat.format(this.datetime);
    }
    public String getRideTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:m a");
        return  dateFormat.format(this.datetime);

    }

    public int getRideId(){return this.ride_id;}
    public String getRideName(){return ride_name;}
//    public String getDatetime(){return this.datetime;}
//    public String getFormattedDate(){return datetime.}
    public String getStartLocation(){return this.startLocation;}
    public String getEndLocation(){return this.endLocation;}
    public String getGroupName(){return this.group_name;}
    public String getDateTimeString() {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy K:mm a");
        return df.format(this.datetime);
    }

    public String toString(){
        return "Ride" + this.ride_id + "datetime" + datetime.toString() + startLocation + endLocation;
    }

}
