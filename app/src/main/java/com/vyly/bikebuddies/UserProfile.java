package com.vyly.bikebuddies;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;

public class UserProfile extends BaseActivity implements View.OnClickListener{
    private String TAG = "UserProfile";
    private User me;
    private boolean done;
    private String rides;
    private String distance;
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.user_profile);
////        ImageButton home = (ImageButton)findViewById(R.id.userButton);
////        home.setOnClickListener(UserProfile.this);
////        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
////            @Override
////            public void onSwipeLeft() {
//////                Toast.makeText(UserProfile.this, "swipeLeft", Toast.LENGTH_SHORT).show();
////            }
////
////            public void onSwipeTop() {
//////                Toast.makeText(UserProfile.this, "top", Toast.LENGTH_SHORT).show();
////            }
////
////            @Override
////            public void onSwipeRight() {
//////                Toast.makeText(UserProfile.this, "right", Toast.LENGTH_SHORT).show();
////                Intent myIntent = new Intent(UserProfile.this, GroupFind.class);
////                startActivity(myIntent);
////            }
////
////            public void onSwipeBottom() {
//////                Toast.makeText(UserProfile.this, "bottom", Toast.LENGTH_SHORT).show();
////            }
////        });
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLayoutInflater().inflate(R.layout.user_profile, frameLayout);

        mDrawerList.setItemChecked(position, true);
        setTitle(listArray[position]);
        me = ((GlobalApplication) this.getApplication()).getUser();
        ImageView prof = (ImageView) findViewById(R.id.imageView);
        TextView name = (TextView)findViewById(R.id.fillName);
        TextView exp = (TextView)findViewById(R.id.fillExp);
        new DownloadImageTask(prof).execute(me.profile_pic_url);
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    getUserHistory();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        name.setText(me.name);
        exp.setText(me.experience);
        while(!this.done){}

        TextView t2 = (TextView)findViewById(R.id.filldistance);
        TextView t1 = (TextView)findViewById(R.id.fillRides);
        t2.setText(distance);
        t1.setText(rides);


    }

    public void getUserHistory(){
        String check = getString(R.string.API_STEM) + "/users/" + me.user_id + "/ride_history";
        String result = Utilities.openURL(check);
        try {
            JSONObject resultObj = new JSONObject(result);
            JSONObject stats = (JSONObject)resultObj.get("statistics");
            distance = stats.getString("total_length").substring(0,3) + " mi";
            rides =  stats.getString("num_rides");
            this.done = true;
        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        Log.i(TAG, "onclick");
        Intent myIntent = new Intent(UserProfile.this,RideCreate.class);
        startActivity(myIntent);
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
