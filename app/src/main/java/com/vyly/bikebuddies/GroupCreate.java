package com.vyly.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

public class GroupCreate extends Activity implements View.OnClickListener{
//TODO make it easier to switch between input fields
    private String TAG = "GroupCreate";

    AutoCompleteTextView atvPlaces;
    User me;
    PlacesTask placesTask;
    ParserTask parserTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_create);
        me = ((GlobalApplication) this.getApplication()).getUser();

        Button home = (Button)findViewById(R.id.createGroupButton);
//        ImageButton home = (ImageButton)findViewById(R.id.groupCreateButton);
        home.setOnClickListener(GroupCreate.this);
        atvPlaces = (AutoCompleteTextView) findViewById(R.id.editText7);

        atvPlaces.setThreshold(1);


        atvPlaces.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });


        atvPlaces.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                atvPlaces.showDropDown();
                return false;
            }
        });

    }

    public void createGroup(){
        try{
            String create = getString(R.string.API_STEM) + "/groups";
            URL url = new URL(create);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();

            EditText name = (EditText) findViewById(R.id.editGroup);
            EditText description = (EditText) findViewById(R.id.editDescripton);
    //        EditText location = (EditText)findViewById(R.id.locationText);
            User me = ((GlobalApplication) this.getApplication()).getUser();

            //Create JSONObject here
            JSONObject postObject = new JSONObject();
            JSONObject groupObject = new JSONObject();
            groupObject.put("name", name.getText());
            groupObject.put("experience", me.experience);
            groupObject.put("description", description.getText());

            groupObject.put("location", atvPlaces.getText());
            postObject.put("group", groupObject);
            Log.i(TAG,postObject.toString());

            //send
            DataOutputStream printout = new DataOutputStream(conn.getOutputStream ());
                    OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(postObject.toString());
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            Log.i(TAG,responseCode+" response");
            InputStream response = conn.getInputStream();
            Log.i("","Response : "+response.toString());
            int bytesRead = -1;
            byte[] buffer = new byte[8*1024];
//            String sHeaderValue = connection.getHeaderField("Your_Header_Key");
            JSONObject group_response = new JSONObject();
            while ((bytesRead = response.read(buffer)) >= 0) {
                String line = new String(buffer, "UTF-8");
                if (line.contains("{" )){
                    int start_brace = line.indexOf("{");
                    int end_brace = line.indexOf("}");
                    group_response = new JSONObject(line.substring(start_brace,end_brace+1));
                    break;
                }
                Log.i("","line : "+line);

//                handleDataFromSync(buffer);
            }
            Log.i(TAG,group_response.toString());
            Log.i(TAG,group_response.getInt("id")+"");
            addToGroup(group_response.getInt("id") + "");
            conn.disconnect();
        }catch(Exception e){
            Log.i(TAG,e.toString());
        }
    }

    @Override
    public void onClick(View v) {

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    createGroup();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        Intent myIntent = new Intent(GroupCreate.this,GroupList.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(myIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addToGroup(String group_id){
        try{
            String create = getString(R.string.API_STEM) + "/users/" + me.user_id + "/groups/" + group_id ;
            URL url = new URL(create);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();

            int responseCode = conn.getResponseCode();
            Log.i(TAG,responseCode+" response");
            conn.disconnect();
        }catch(Exception e){
            Log.i(TAG,e.toString());
        }
    }
    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch(Exception e){
//            Log.d("Exception while downloading url", e.toString());
        } finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // API KEY
            String key = "key=AIzaSyBwhJskWAekAXnGBRsNAFbMeqWh2HNbvLM";

            String input="";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input+"&"+types+"&"+sensor+"&"+key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;

            try {
                data = downloadUrl(url);
                System.out.println(data);
            } catch(Exception e){
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);
                places = placeJsonParser.parse(jObject);
            } catch(Exception e){
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[] { "description"};
            int[] to = new int[] { android.R.id.text1 };

            String[] places = new String[result.size()];
            for (int i=0; i < result.size(); i++) {
                HashMap<String, String> x = result.get(i);
                places[i] = (x.get("description"));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                    android.R.layout.simple_dropdown_item_1line, places);
//            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);
            atvPlaces.setAdapter(adapter);

        }
    }
}
