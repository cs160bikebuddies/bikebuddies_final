package com.vyly.bikebuddies;

import java.io.Serializable;

/**
 * Created by vyly on 12/6/15.
 */
@SuppressWarnings("serial") // hide compiler warning
public class User implements Serializable {
    int user_id;
    String name;
    String profile_pic_url;
    String experience;
    String json;

    public User(int user_id, String name, String profile_pic_url,String experience,String json ){
        this.user_id = user_id;
        this.name = name;
        this.profile_pic_url= profile_pic_url;
        this.experience = experience;
    }

    public String toString(){
        return json;
    }
}
