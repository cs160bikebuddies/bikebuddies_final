package com.vyly.bikebuddies;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vyly on 12/5/15.
 */
@SuppressWarnings("serial") // hide compiler warning
public class Group implements Serializable{
    private String group_id;
    private String group_name;
    String experience;
    String description;
    ArrayList<String> member_profile_pics;
    public Group(String group_id, String group_name, ArrayList<String> member_profile_pics){
        this.group_id = group_id;
        this.group_name = group_name;
        this.member_profile_pics = member_profile_pics;
    }
    public Group(String group_id, String group_name, ArrayList<String> member_profile_pics,String description){
        this.group_id = group_id;
        this.group_name = group_name;
        this.member_profile_pics = member_profile_pics;
        this.description = description;
    }
    public String getDescription(){return description;}
    public String getGroupId(){return group_id;}
    public String getGroupName(){return group_name;}
    public ArrayList<String> member_profile_pics(){return member_profile_pics;}
    public int numMembers(){return member_profile_pics.size();}
}
