package com.vyly.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements View.OnClickListener{

    private GestureDetectorCompat mDetector;
    private String TAG = "MainActivity";
    TextView user;
    TextView pass;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView login = (TextView) findViewById(R.id.loginView);
        TextView signup = (TextView) findViewById(R.id.signUpView);
        user = (TextView) findViewById(R.id.userView);
        pass = (TextView) findViewById(R.id.password);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i(TAG,"click");
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            goToProfile();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();

        }});

    }

    public void goToProfile(){
        String check = getString(R.string.API_STEM) + "/users/" + user.getText() + "/" + pass.getText()+"/valid";
        String result = Utilities.openURL(check);
        try {
            JSONObject resultObj = new JSONObject(result);
            boolean isValid = (boolean)resultObj.get("valid");

            if(isValid){
                JSONObject userObj = (JSONObject) resultObj.get("user");
                int id = (int)userObj.get("id");
                String name = userObj.getString("name");
                String profile = userObj.getString("profile_pic_url");
                String exp = userObj.getString("experience");
                User me = new User(id, name, profile,exp, result);
                setUser(me);
                Intent home = new Intent(getApplicationContext(),UserProfile.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("user", me);
//                home.putExtras(bundle);
                startActivity(home);
            }
            else{
                Log.i(TAG,"wrong pass");
            }

        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
    }
    public void setUser(User u){
        ((GlobalApplication) this.getApplication()).setUser(u);
    }
    @Override
    public void onClick(View v) {
        Log.i(TAG,"onclick");
        Intent myIntent = new Intent(MainActivity.this, UserCreateProfile.class);
        startActivity(myIntent);

    }
}