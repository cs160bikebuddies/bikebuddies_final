package com.vyly.bikebuddies;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class GroupList extends BaseActivity implements View.OnClickListener{
    public Context context;
    private int user_id;
    private String TAG = "GroupList";
    private ArrayList<Group> gList; //List of groups for this user
    private boolean done = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_group_list, frameLayout);
        mDrawerList.setItemChecked(position, true);
        setTitle(listArray[position]);
        User user = ((GlobalApplication) this.getApplication()).getUser();
        user_id = user.user_id;
        gList = new ArrayList<Group>();
//        Button to_find = (Button) findViewById(R.id.button);
//        to_find.setOnClickListener(
//                new View.OnClickListener() {
//                    public void onClick(View v) {
//                        Intent i = new Intent(GroupList.this, GroupFind.class);
//
//                        startActivity(i);
//                    }
//                });
        View addButton = findViewById(R.id.add_button);
        addButton.setOutlineProvider(new ViewOutlineProvider() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void getOutline(View view, Outline outline) {
                int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                outline.setOval(0, 0, diameter, diameter);
            }
        });
        addButton.setClipToOutline(true);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(GroupList.this, GroupCreate.class);

                startActivity(i);
            }
        });

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    pullGroups();
//                  }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

        while(!this.done){

        }
        //TODO make this less janky
        if(this.done){
            setListView();
        }



    }
    private void pullGroups(){
        String getGroups = getString(R.string.API_STEM) + "/users/" + user_id;
        String result = Utilities.openURL(getGroups);
        try {
            JSONObject resultObj = new JSONObject(result);
            JSONArray groups = resultObj.getJSONArray("groups");
            int numGroups = groups.length() > 10 ? 10 : groups.length();
            for(int i=0; i < numGroups; i++) {
                JSONObject groupObj = groups.getJSONObject(i);
                String group_id = groupObj.getString("id");
                String group_name = groupObj.getString("name");
                String description = null;
                if(groupObj.has("description")) {
                    description = groupObj.getString("description");
                }
                ArrayList<String> member_pics =  getGroupMembers(group_id);
                gList.add(new Group(group_id,group_name,member_pics,description));
            }
            Log.i(TAG,gList.toString());
            this.done = true;


        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
    }
    private void setListView(){
        ListView yourListView= (ListView) findViewById(R.id.groupListView);
        LayoutInflater inflater = getLayoutInflater();
        GroupListAdapter customAdapter = new GroupListAdapter(this, R.layout.grouplistrow, gList);
        yourListView.setAdapter(customAdapter);
    }

    private ArrayList<String> getGroupMembers(String group_id){
        String request = getString(R.string.API_STEM) +"/groups/" + group_id + "/users";
        String result = Utilities.openURL(request);
        ArrayList<String> member_pics = new ArrayList<>();
        try {
            JSONArray resultObj = new JSONArray(result);

            int numMembers = resultObj.length() > 10 ? 10 : resultObj.length();
            for(int i=0; i < numMembers; i++) {
                JSONObject member = resultObj.getJSONObject(i);
                String member_id = member.getString("id");
                String profile_pic_url = member.getString("profile_pic_url");
                member_pics.add(profile_pic_url);
            }

        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
        return member_pics;
    }

    public static void goToGroupDetails(Context c,Group thisgroup){
        Intent home = new Intent(c,GroupDetails.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("group", thisgroup);
        home.putExtras(bundle);
//        Log.i("Selecting", q.getCoordinates().getLongitude() + "");
//        myIntent.putExtra("qlat", q.getCoordinates().getLatitude());
//        myIntent.putExtra("qlng", q.getCoordinates().getLongitude());
//        myIntent.putExtra("mag", q.getMag());
//        myIntent.putExtra("source","map");
//        myIntent.putExtra("type",PhoneListenerService.MANUAL_MAP);
        c.startActivity(home);

    }
    @Override
    public void onClick(View v) {
        Intent myIntent = new Intent(GroupList.this,GroupCreate.class);
        startActivity(myIntent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.action_search:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                Intent i = new Intent(GroupList.this, GroupFind.class);
                startActivity(i);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

//    public String[] getGroups() {
//        try {
//            String getGroups = "http://bikebuddies.herokuapp.com/api/v1/users/" + user_id;
//            String result = Utilities.openURL(getGroups);
//
//            try {
//                JSONObject resultObj = new JSONObject(result);
//                JSONArray groups = resultObj.getJSONArray("groups");
//                String[] names = new String[groups.length()];
//
//                int numGroups = groups.length() > 10 ? 10 : groups.length();
//                for (int i = 0; i < numGroups; i++) {
//                    JSONObject groupObj = groups.getJSONObject(i);
//                    String group_name = groupObj.getString("name");
//                    names[i] = group_name;
//                }
//                return names;
//            } catch (JSONException e) {
//                Log.e("JSONException", "Error: " + e.toString());
//                return new String[1];
//            }
//        } catch (Exception e) {
//            // Default
////            Log.e("Exception", "Error: " + e.toString());
//            return new String[]{"Group", "School", "Work", "Friends"};
//        }
//    }
}
