package com.vyly.bikebuddies;

/**
 * Created by vyly on 11/29/15.
 */
@SuppressWarnings("serial") // hide compiler warning
public class RideTest{
    private int ride_id;
    private String ride_name;
    private String datetime;
    private String startLocation;
    private String endLocation;
    private String group_name;

    public RideTest(int ride_id, String ride_name,String group_name){
        this.ride_id = ride_id;
        this.ride_name= ride_name;
        this.datetime = "12/15/16";
        this.group_name = group_name;
//        Log.i("new ride", group_name + ride_name);

    }

    public int getRideId(){return this.ride_id;}
    public String getRideName(){return ride_name;}
    public String getDatetime(){return this.datetime;}
    //    public String getFormattedDate(){return datetime.}
    public String getStartLocation(){return this.startLocation;}
    public String getEndLocation(){return this.endLocation;}
    public String getGroupName(){return this.group_name;}

    public String toString(){
        return "Ride" + this.ride_id + "datetime" + datetime.toString() + startLocation + endLocation;
    }

}
