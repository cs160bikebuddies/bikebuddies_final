package com.vyly.bikebuddies;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class JSONMapParser {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    // constructor
    public JSONMapParser() {
    }

    public String getJSONFromUrl(String url) {

        // Making HTTP request
        URL mapsURL;
        HttpURLConnection urlConnection;
        try {
            mapsURL = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "{}";
        }

        try {
            urlConnection = (HttpURLConnection) mapsURL.openConnection();
            urlConnection.connect();

            try {
                InputStream in = urlConnection.getInputStream();

                BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                StringBuilder responseStrBuilder = new StringBuilder();

                String inputStr;
                while ((inputStr = streamReader.readLine()) != null) {
                    responseStrBuilder.append(inputStr);
                }
                json = responseStrBuilder.toString();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }
            return json;
        } catch (IOException e) {
            e.printStackTrace();
            return "{}";
        }
    }
}