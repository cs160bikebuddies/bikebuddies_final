package com.vyly.bikebuddies;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vyly on 11/29/15.
 */
public class GroupListAdapter extends ArrayAdapter<Group> {
    Context context;

    public GroupListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public GroupListAdapter(Context context, int resource, List<Group> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.grouplistrow, null);
        }

        final Group p = getItem(position);

        if (p != null) {
            TextView group_name = (TextView) v.findViewById(R.id.group_name);
            TextView num_members = (TextView) v.findViewById(R.id.num_members);
            ImageView member1 = (ImageView) v.findViewById(R.id.member1);
            ImageView member2 = (ImageView) v.findViewById(R.id.member2);
            ImageView member3 = (ImageView) v.findViewById(R.id.member3);
            ImageView member4 = (ImageView) v.findViewById(R.id.member4);
            ArrayList<String> member_pics = p.member_profile_pics();
            int numMembers = p.numMembers();

            if (group_name!= null) {
                group_name.setText(p.getGroupName());
            }

            if (num_members!= null) {
                num_members.setText(p.numMembers() + " members");

            }

            if (member1 != null && numMembers>=1) {
                new DownloadImageTask(member1).execute(member_pics.get(0));
            }

            if (member2 != null&& numMembers>=2 ) {
                new DownloadImageTask(member2).execute(member_pics.get(1));


            }
            if (member3 != null&& numMembers>=3) {
                new DownloadImageTask(member3).execute(member_pics.get(2));


            }
            if (member4 != null&& numMembers>=4) {
                new DownloadImageTask(member4).execute(member_pics.get(3));
            }

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    GroupList.goToGroupDetails(context, p);
//                    Toast.makeText(context, p.getGroupName(), Toast.LENGTH_SHORT).show();
                }
            });
        }


        return v;
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}