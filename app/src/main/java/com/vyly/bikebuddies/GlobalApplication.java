package com.vyly.bikebuddies;

import android.app.Application;

/**
 * Created by vyly on 12/5/15.
 */
public class GlobalApplication extends Application {
    private String user_id = "1";
    private User user;
    public User getUser() {
        return user;
    }

    public void setUser( User someVariable) {
        this.user = someVariable;
    }
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id( String someVariable) {
        this.user_id = someVariable;
    }
}