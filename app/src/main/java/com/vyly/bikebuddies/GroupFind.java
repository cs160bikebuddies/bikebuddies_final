package com.vyly.bikebuddies;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GroupFind extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private String TAG = "GroupFind";
    private ArrayList<String> locations;
    private ArrayList<Group> groups;
    private GoogleMap mMap;
    private boolean done = false;
    private boolean firstclick = false;
    private ArrayList<Marker> markerList;
    private User me;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_find);
        me = ((GlobalApplication) this.getApplication()).getUser();
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();
        mapFragment.getMapAsync(this);
        locations = new ArrayList<>();
        groups = new ArrayList<>();
        markerList = new ArrayList<>();
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    getGroups();
//                  }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        while(!this.done){}
        Log.i(TAG,locations.toString());
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                int indx = Integer.parseInt(marker.getId().substring(1));

                Intent home = new Intent(getApplicationContext(),GroupDetails.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("group", groups.get(indx));
                home.putExtras(bundle);
                startActivity(home);


            }
        });
        Geocoder geocoder = new Geocoder(getBaseContext());
        List<Address> addresses;

        try {
            for (int i = 0;i < locations.size();i++ ) {
                addresses = geocoder.getFromLocationName(locations.get(i), 1);
                if (addresses.size() > 0) {
                    double latitude = addresses.get(0).getLatitude();
                    double longitude = addresses.get(0).getLongitude();

                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 8));

                    if (groups.get(i).getDescription() != "null") {
                        mMap.addMarker(new MarkerOptions()
                                .title(groups.get(i).getGroupName())
                                .snippet(groups.get(i).getDescription())
                                .position(new LatLng(latitude, longitude)));

                    } else {
                        mMap.addMarker(new MarkerOptions()
                                .title(groups.get(i).getGroupName())
                                .position(new LatLng(latitude, longitude)));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }


    }
    @Override
    public boolean onMarkerClick(final Marker marker) {
        int indx = Integer.parseInt(marker.getId().substring(1));

//        AlertDialog alertDialog = new AlertDialog.Builder(GroupFind.this).create();
//        alertDialog.setTitle("Add");
//        alertDialog.setMessage("Do you want to join?");
//        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//        alertDialog.show();
        return false;

    }
    @Override
    public void onMapReady(GoogleMap map) {
//        LatLng sydney = new LatLng(-33.867, 151.206);
        map.setMyLocationEnabled(true);
//        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

    }
    private void getGroups(){
        Log.i(TAG, "get group members");
        String request = getString(R.string.API_STEM) +"/groups/" + me.experience +"/filter/" + me.user_id;
        String result = Utilities.openURL(request);
        try {
            JSONArray resultObj = new JSONArray(result);

            int numGroups = resultObj.length() > 10 ? 10 : resultObj.length();
            for(int i=0; i < numGroups; i++) {
                JSONObject groupObj = resultObj.getJSONObject(i);
                String group_id = groupObj.getString("id");
                String group_name = groupObj.getString("name");
                String description = null;
                if(groupObj.has("description")) {
                    description = groupObj.getString("description");
                }
                locations.add(groupObj.getString("location"));
                groups.add(new Group(group_id, group_name, null, description));
            }
            this.done = true;

        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    private void setUpMapIfNeeded() {
        if (this.mMap != null) {
            return;
        }
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        if (mMap == null) {
            return;
        }
        // Initialize map options. For example:
        // mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }
}