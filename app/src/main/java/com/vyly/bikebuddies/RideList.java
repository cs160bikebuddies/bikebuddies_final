package com.vyly.bikebuddies;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class RideList extends BaseActivity {
    private boolean done = false;
    private ArrayList<Ride> rList; //List of Rides for this user
    public Context context;
    private int user_id;
    private String TAG = "RideList";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLayoutInflater().inflate(R.layout.activity_ride_list, frameLayout);

        /**
         * Setting title and itemChecked
         */
        mDrawerList.setItemChecked(position, true);
        setTitle(listArray[position]);
//        setContentView(R.layout.activity_ride_list);
        User user = ((GlobalApplication) this.getApplication()).getUser();
        user_id = user.user_id;
        rList = new ArrayList<Ride>();
        context = getApplicationContext();

        View addButton = findViewById(R.id.add_button);
        addButton.setOutlineProvider(new ViewOutlineProvider() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void getOutline(View view, Outline outline) {
                int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                outline.setOval(0, 0, diameter, diameter);
            }
        });
        addButton.setClipToOutline(true);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RideList.this, RideCreate.class);
                startActivity(i);
            }
        });


        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    pullRides();

//                  }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        while(!this.done){

        }
        //TODO make this less janky
        if(this.done){
            setListView();
        }
    }
    public Date getDate(String queryResponse) {
        //TODO fix date
        DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date = new Date();
        try {
            date = iso8601Format.parse(queryResponse);
        } catch (ParseException e) {
            Log.e("TAG", "Parsing ISO8601 datetime failed", e);
        }

        return date;
    }

    public String formatDateDB(String queryResponse) {
        DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        Date date = new Date();
        try {
            date = iso8601Format.parse(queryResponse);
        } catch (ParseException e) {
            Log.e("TAG", "Parsing ISO8601 datetime failed", e);
        }

        long when = date.getTime();
        int flags = 0;
        flags |= android.text.format.DateUtils.FORMAT_SHOW_TIME;
        flags |= android.text.format.DateUtils.FORMAT_SHOW_DATE;
        flags |= android.text.format.DateUtils.FORMAT_ABBREV_MONTH;
        flags |= android.text.format.DateUtils.FORMAT_SHOW_YEAR;

        return android.text.format.DateUtils.formatDateTime(context,
                when + TimeZone.getDefault().getOffset(when), flags);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ride_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void pullRides(){
        String rideRequest = getString(R.string.API_STEM) + "/users/" + user_id + "/rides";
        String result = Utilities.openURL(rideRequest);
        parseJSONRide(result);
    }

    public String pullGroup(String group_id){
        String rideRequest = getString(R.string.API_STEM) + "/groups/" + group_id;
        String result = Utilities.openURL(rideRequest);
        return parseJSONGroup(result);

    }

    public String parseJSONGroup(String result) {
        //parse JSON data and add it to the rideList
        try {
            JSONObject group = new JSONObject(result);
            return group.getString("name");
        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
        return "";
    }

    public void parseJSONRide(String result) {

        try {
            JSONArray rideArray = new JSONArray(result);
            int numRides = rideArray.length() > 10 ? 10 : rideArray.length();
            for(int i=0; i < numRides; i++) {
                JSONObject rideObj = rideArray.getJSONObject(i);
                String ride_name = rideObj.getString("ride_name");
                Date ride_startdatetime = getDate(rideObj.getString("ride_start_time"));
                int group_id = Integer.parseInt(rideObj.getString("group_id"));
                String group_name = pullGroup(rideObj.getString("group_id"));
                String start_location = rideObj.getString("start_location");
                String dst_location = rideObj.getString("dest_location");
                int ride_id = Integer.parseInt(rideObj.getString("id"));
                rList.add(new Ride(ride_id,ride_name,ride_startdatetime,group_name,start_location,dst_location));
            }
            this.done = true;

        } catch (JSONException e) {
            Log.e("JSONException", "Error: " + e.toString());
        }
    }

    private void setListView(){
        ListView yourListView= (ListView) findViewById(R.id.rideListView);
// get data from the table by the ListAdapter
        LayoutInflater inflater = getLayoutInflater();
//        View header = inflater.inflate(R.layout.header, yourListView, false);
//        yourListView.addHeaderView(header, null, false);
        ListAdapter customAdapter = new ListAdapter(this, R.layout.ridelistrow, rList);
        yourListView.setAdapter(customAdapter);
    }


    public static void goToRide(Context c,Ride r){
        Intent myIntent = new Intent(c,RideDetails.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("rideObj", r);
        myIntent.putExtras(bundle);
        c.startActivity(myIntent);

    }

}
