package com.vyly.bikebuddies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by vyly on 11/29/15.
 */
public class ListAdapter extends ArrayAdapter<Ride> {
    Context context;

    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public ListAdapter(Context context, int resource, List<Ride> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.ridelistrow, null);
        }

        final Ride p = getItem(position);

        if (p != null) {
            TextView tt1 = (TextView) v.findViewById(R.id.ride_name);
            TextView tt2 = (TextView) v.findViewById(R.id.group_name);
            TextView r_date = (TextView) v.findViewById(R.id.ride_date);
            TextView r_time = (TextView) v.findViewById(R.id.ride_time);

            if (tt1 != null) {
                tt1.setText(p.getRideName() );
            }

            if (tt2 != null) {
                tt2.setText(p.getGroupName());
            }

            if (r_date != null) {
                r_date.setText(p.getRideDate());
            }

            if (r_time != null) {
                r_time.setText(p.getRideTime());
            }

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    RideList.goToRide(context, p);
                }
            });
        }


        return v;
    }



}