package com.vyly.bikebuddies;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

public class RideDuringMetrics extends AppCompatActivity {

    private static final String END_RIDE = "/end_ride";
    private GoogleApiClient mApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_during_metrics);
        registerReceiver(uiUpdated, new IntentFilter("METRICS_UPDATED_PHONE"));

//        RelativeLayout home = (RelativeLayout) findViewById(R.id.layout);
//        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
//            @Override
//            public void onSwipeRight() {
//                Intent myIntent = new Intent(RideDuringMetrics.this, RideList.class);
//                startActivity(myIntent);
//            }
//
//            @Override
//            public void onSwipeLeft() {
//                Intent myIntent = new Intent(RideDuringMetrics.this, RideDuringMap.class);
//                startActivity(myIntent);
//            }
//        });

        Intent intent = getIntent();
        final String src_location = intent.getStringExtra("src_location");
        final String dst_location = intent.getStringExtra("dst_location");

        Button to_map = (Button) findViewById(R.id.map_button);
        to_map.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(RideDuringMetrics.this, RideDuringMap.class);
                        i.putExtra("src_location", src_location);
                        i.putExtra("dst_location", dst_location);
                        startActivity(i);
                    }
                });

//        Button to_end = (Button) findViewById(R.id.end_button);
//        to_end.setOnClickListener(
//                new View.OnClickListener() {
//                    public void onClick(View v) {
//                        Intent i = new Intent(RideDuringMetrics.this, RideList.class);
//                        startActivity(i);
//
//                        mApiClient.connect();
//                        sendMessage(END_RIDE);
//                    }
//                });
    }

    private BroadcastReceiver uiUpdated = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            TextView clock = (TextView) findViewById(R.id.textView4);
            TextView hr = (TextView) findViewById(R.id.textView3);
            TextView d = (TextView) findViewById(R.id.textView2);
            TextView v = (TextView) findViewById(R.id.fillRide);

            clock.setText(intent.getStringExtra("clock"));
            v.setText(intent.getStringExtra("velocity"));
            d.setText(intent.getStringExtra("distance"));
            hr.setText(intent.getStringExtra("hr"));
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ride_during_metrics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(uiUpdated);
        super.onDestroy();
    }

//    private void sendMessage( final String path) {
//        new Thread( new Runnable() {
//            @Override
//            public void run() {
//                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
//                for (Node node : nodes.getNodes()) {
//                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
//                            mApiClient, node.getId(), path, null ).await();
//                }
//            }
//        }).start();
//    }
}
