package com.vyly.bikebuddies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

public class RideDetails extends AppCompatActivity{
    private String TAG = "RideDetails";
    private String start_location;
    private String end_location;
    private String name;
    private String datetime;
    private String group_id = "1";

    private static final String START_RIDE_ACTIVITY = "/start_ride_service";;
    private GoogleApiClient mApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_details);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        Ride r = (Ride) bundle.getSerializable("rideObj");
        Log.i(TAG, r.toString());

//        me = ((GlobalApplication) this.getApplication()).getUser();
//        group_id = intent.getStringExtra("group_id");

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setTitle(r.getRideName());
        name = r.getRideName();

        TextView riders = (TextView)findViewById(R.id.fillRide);
        riders.setText(r.getGroupName());

        TextView date = (TextView)findViewById(R.id.fillDate);
        date.setText(r.getRideDate());
        String _date = r.getRideDate();

        TextView time = (TextView)findViewById(R.id.fillTime);
        time.setText(r.getRideTime());
        String _time = r.getRideTime();
        datetime = _date + " " + _time;

        TextView start = (TextView)findViewById(R.id.fillStart);
        start.setText(r.getStartLocation());
        start_location = r.getStartLocation();

        final TextView dst = (TextView)findViewById(R.id.fillDst);
        dst.setText(r.getEndLocation());
        end_location = r.getEndLocation();

        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        Button home = (Button) findViewById(R.id.startButton);
        home.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(RideDetails.this, RideDuringMetrics.class);
                        i.putExtra("src_location", start_location);
                        i.putExtra("dst_location", end_location);
                        startActivity(i);

                        mApiClient.connect();
                        sendMessage(START_RIDE_ACTIVITY);

                    }
                });

//        ImageButton home = (ImageButton)findViewById(R.id.rideDetailsButton);


//        home.setOnClickListener(RideDetails.this);

//        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {

//            @Override
//            public void onSwipeRight() {
//                Intent sendToWatchIntent = new Intent(getApplicationContext(), PhoneSenderService.class);
//                Bundle b = new Bundle();
//                b.putDouble("ride_id", 123456);
//                b.putString("type", PhoneSenderService.START_RIDE_ACTIVITY);
//                sendToWatchIntent.putExtras(b);
//                Log.i(TAG, "send to PhoneSender");
//                getApplicationContext().startService(sendToWatchIntent);
//
//                Intent myIntent = new Intent(RideDetails.this, RideDuringMap.class);
//                Log.i(TAG,"switch to Map" );
//                startActivity(myIntent);
//            }
//        });

//        home.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                Log.v("ImageButton", "Clicked!");
//                Toast.makeText(RideDetails.this, "ImP", Toast.LENGTH_SHORT).show();
//
//            }
//        });

    }

//    private void addButtonListener() {
//        ImageButton imgButton = (ImageButton) findViewById(R.id.rideDetailsButton);
//        imgButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(RideDetails.this,"ImageButton is working!", Toast.LENGTH_SHORT).show();
//
//            }
//
//        });
//
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ride_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendMessage( final String path) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for (Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, null ).await();
                }
            }
        }).start();
    }

}
