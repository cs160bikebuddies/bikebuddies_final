package com.vyly.bikebuddies;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by vyly on 12/5/15.
 * For grid view
 */
public class CustomAdapter extends ArrayAdapter<String> {

    private Activity context;
    private ArrayList<String> pics;
    private ArrayList<String> names;
    private ArrayList<Integer> ids;

    private int pos;

    public CustomAdapter(Activity context, ArrayList<String> names, ArrayList<String> pics,ArrayList<Integer>ids) {
        super(context, R.layout.grid_item_layout, names);
        this.context = context;
        this.names = names;
        this.pics = pics;
        this.ids = ids;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater i = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View) i.inflate(R.layout.grid_item_layout, parent, false);
        }

        TextView t = (TextView) convertView.findViewById(R.id.label);
        ImageView member1 = (ImageView) convertView.findViewById(R.id.image);
        t.setText(names.get(position));
        new DownloadImageTask(member1).execute(pics.get(position));
        final int user_id = ids.get(position);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                GroupDetails.goToUserProfile(context,user_id);
            }
        });
        return convertView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}