package com.vyly.bikebuddies;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class RideCreate extends AppCompatActivity {
    Calendar myCalendar;
    EditText edittext;
    EditText timetext;

    AutoCompleteTextView atvPlaces;
    AutoCompleteTextView atvPlaces2;
    PlacesTask placesTask;
    ParserTask parserTask;
    boolean done;
    ArrayList<String> names;

    String group_name;

    DatePickerDialog.OnDateSetListener date;
    private String TAG = "RideCreate";

    String _datetime;
    String _start_location;
    String _end_location;
    String _name;
    Ride ride;

    String user_id = "1";
    String group_id = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_create);

        try{
            Intent i = getIntent();
            this.group_name = i.getStringExtra("group_name");
            Log.i(TAG,this.group_name +"group we got");

        }catch(Exception e){
            Log.e(TAG,e.toString());
        }

        edittext = (EditText)findViewById(R.id.dateText);
        timetext = (EditText)findViewById(R.id.timeText);
        names = new ArrayList<>();

        User me = ((GlobalApplication) this.getApplication()).getUser();
        user_id = String.valueOf(me.user_id);

        Button home = (Button) findViewById(R.id.startButton);
        home.setOnClickListener(
                new View.OnClickListener() {
                    EditText name = (EditText) findViewById(R.id.editText3);
                    EditText start = (EditText) findViewById(R.id.editText7);
                    EditText end = (EditText) findViewById(R.id.editText6);
                    Spinner group =(Spinner) findViewById(R.id.spinner);

                    public void onClick(View v) {
                        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy K:mm a");
                        Intent i = new Intent(RideCreate.this, RideDetails.class);

                        try {
                            ride = new Ride(0, name.getText().toString(), df.parse(edittext.getText().toString() + " " + timetext.getText().toString()),
                                            group.getSelectedItem().toString(), start.getText().toString(), end.getText().toString());

//                            Log.i(TAG,edittext.getText().toString());
//                            Log.i(TAG,timetext.getText().toString());
//                            Log.i(TAG,end.getText().toString());
//                            Log.i(TAG,start.getText().toString());

                            Bundle bundle = new Bundle();
                            bundle.putSerializable("rideObj", ride);
                            i.putExtras(bundle);
                            i.putExtra("group_id", group_id);
                            startActivity(i);

                        } catch (ParseException e) {
                            e.printStackTrace();
                            return;
                        }

                        _name = ride.getRideName();
                        String _date = ride.getRideDate();
                        String _time = ride.getRideTime();
//                        _datetime = ride.getDateTimeString();
                        _datetime = _date + _time;
                        _start_location = ride.getStartLocation();
                        _end_location = ride.getEndLocation();

                        Thread thread = new Thread(new Runnable(){
                            @Override
                            public void run() {
                                try {
                                    createRide();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();

                    }
                });

        myCalendar = Calendar.getInstance();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RideCreate.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        timetext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(RideCreate.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String meridian = selectedHour < 12? "AM" : "PM";
                        if(selectedMinute == 0){
                            timetext.setText(selectedHour % 12 + ":00 "  + meridian);

                        }
                        else {
                            timetext.setText(selectedHour % 12 + ":" + String.format("%02d ", selectedMinute) + meridian);
                        }
                    }
                }, hour, minute,false);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        Spinner dropdown = (Spinner) findViewById(R.id.spinner);
//        GroupList g = new GroupList();
//        names = new String[]{"Group", "School", "Work", "Friends"};


        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    getGroups();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, names);

        while(this.done == false){

        }
        Log.i("RIDECREATE", names.toString());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, this.names);
        dropdown.setAdapter(adapter);
        try {
            if(this.group_name!= null){
                dropdown.setSelection(getIndex(dropdown, this.group_name));
            }
        }catch (Exception e){
            Log.e(TAG,e.toString());
        }


        atvPlaces = (AutoCompleteTextView) findViewById(R.id.editText7);
        atvPlaces2 = (AutoCompleteTextView) findViewById(R.id.editText6);
        atvPlaces.setThreshold(1);
        atvPlaces2.setThreshold(1);

        atvPlaces.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        atvPlaces2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        atvPlaces.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                atvPlaces.showDropDown();
                return false;
            }
        });

        atvPlaces2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                atvPlaces2.showDropDown();
                return false;
            }
        });
    }

    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
            Log.d(TAG,spinner.getItemAtPosition(i).toString() + "  i" + i);

        }
        Log.d(TAG,index+" return");
        return index;
    }
    public void getGroups() {
        try {
            String getGroups = "http://bikebuddies.herokuapp.com/api/v1/users/" + user_id;
            String result = Utilities.openURL(getGroups);

            try {
                JSONObject resultObj = new JSONObject(result);
                JSONArray groups = resultObj.getJSONArray("groups");
//                this.names = new String[groups.length()];

                int numGroups = groups.length() > 10 ? 10 : groups.length();
                for (int i = 0; i < numGroups; i++) {
                    JSONObject groupObj = groups.getJSONObject(i);
                    String group_name = groupObj.getString("name");
                    group_id = groupObj.getString("id");
                    this.names.add(group_name);
                }
                this.done = true;
            } catch (JSONException e) {
                Log.e("JSONException", "Error: " + e.toString());
//                return new String[1];
            }
        } catch (Exception e) {
            // Default
//            Log.e("Exception", "Error: " + e.toString());
            this.names = new ArrayList<String>();
//            names.addAll(Arrays.asList("Group", "School", "Work", "Friends"));
        }
    }
    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch(Exception e){
//            Log.d("Exception while downloading url", e.toString());
        } finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // API KEY
            String key = "key=AIzaSyBwhJskWAekAXnGBRsNAFbMeqWh2HNbvLM";

            String input="";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input+"&"+types+"&"+sensor+"&"+key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;

            try {
                data = downloadUrl(url);
                System.out.println(data);
            } catch(Exception e){
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);
                places = placeJsonParser.parse(jObject);
            } catch(Exception e){
                Log.d("Exception", e.toString());
            }
            return places;
        }


        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[] { "description"};
            int[] to = new int[] { android.R.id.text1 };

            String[] places = new String[result.size()];
            for (int i=0; i < result.size(); i++) {
                HashMap<String, String> x = result.get(i);
                places[i] = (x.get("description"));
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                    android.R.layout.simple_dropdown_item_1line, places);
//            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);
            atvPlaces.setAdapter(adapter);
            atvPlaces2.setAdapter(adapter);
        }
    }

    private void updateLabel() {

        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittext.setText(sdf.format(myCalendar.getTime()));
    }

//    @Override
//    public void onClick(View v) {
//        Intent myIntent = new Intent(RideCreate.this,RideDetails.class);
//        startActivity(myIntent);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ride_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createRide(){
        try{
            String create = getString(R.string.API_STEM) + "/groups/" + group_id + "/ride/" + user_id;
            URL url = new URL(create);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setInstanceFollowRedirects(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();

            //Create JSONObject here
            JSONObject postObject = new JSONObject();
            JSONObject rideObject = new JSONObject();
            rideObject.put("ride_start_time", _datetime);
            rideObject.put("start_location", _start_location);
            rideObject.put("dest_location", _end_location);
            rideObject.put("length_miles", "3.3"); // PUT this when ride is over.
            rideObject.put("ride_name", _name);

            postObject.put("ride", rideObject);
            Log.i(TAG,postObject.toString());

            //send
            DataOutputStream printout = new DataOutputStream(conn.getOutputStream ());
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(postObject.toString());
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            Log.i(TAG,responseCode+" response");
            InputStream response = conn.getInputStream();
            InputStream errors = conn.getErrorStream();
            Log.i("","Response : "+response.toString());
//            Log.i("", "Errors : "+errors.toString());
            int bytesRead = -1;
            byte[] buffer = new byte[8*1024];
//            String sHeaderValue = connection.getHeaderField("Your_Header_Key");
            JSONObject group_response = new JSONObject();
            while ((bytesRead = response.read(buffer)) >= 0) {
                String line = new String(buffer, "UTF-8");
                if (line.contains("{" )){
                    int start_brace = line.indexOf("{");
                    int end_brace = line.indexOf("}");
                    group_response = new JSONObject(line.substring(start_brace,end_brace+1));
                    break;
                }
                Log.i("","line : "+line);

//                handleDataFromSync(buffer);
            }
            Log.i(TAG,group_response.toString());
            Log.i(TAG, group_response.getInt("id") + "");
            conn.disconnect();
        }catch(Exception e){
            Log.i(TAG,e.toString());
        }
    }

}
