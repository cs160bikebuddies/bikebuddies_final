package com.vyly.bikebuddies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class UserCreateProfile extends AppCompatActivity implements View.OnClickListener{
    public String TAG = "UserCreateProfile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_create_profile);
        ImageButton home = (ImageButton)findViewById(R.id.userCreateProfile);
        home.setOnClickListener(UserCreateProfile.this);
        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
//                Toast.makeText(UserCreateProfile.this, "swipeLeft", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeTop() {
//                Toast.makeText(UserCreateProfile.this, "top", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSwipeRight() {
//                Toast.makeText(UserCreateProfile.this, "right", Toast.LENGTH_SHORT).show();
//                Intent myIntent = new Intent(UserCreateProfile.this, UserProfile.class);
//                startActivity(myIntent);
            }

            public void onSwipeBottom() {
//                Toast.makeText(UserCreateProfile.this, "bottom", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        Log.i(TAG, "onclick");
        Intent myIntent = new Intent(UserCreateProfile.this, UserProfile.class);
        startActivity(myIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_create_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
