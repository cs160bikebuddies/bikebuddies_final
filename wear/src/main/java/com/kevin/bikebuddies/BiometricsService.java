package com.kevin.bikebuddies;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import java.sql.Time;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class BiometricsService extends Service {

    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity

    private float velocity = 0.00F;
    private float distance = 0.01F;
    private float heartRate = 60.00F;

    private long start;

    private static final int INTERVAL = 1000;
    private Timer timer;

    @Override
    public void onCreate() {
        super.onCreate();
        registerReceiver(kill, new IntentFilter("KILL"));

        velocity = 0.00F;
        distance = 0.01F;
        heartRate = 60.00F;

        start = SystemClock.elapsedRealtime();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        mSensorManager.registerListener(
                mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL
        );
        mSensorManager.registerListener(
                mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE),
                SensorManager.SENSOR_DELAY_NORMAL
        );
        mSensorManager.registerListener(
                mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER),
                SensorManager.SENSOR_DELAY_NORMAL
        );
        mSensorManager.registerListener(
                mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR),
                SensorManager.SENSOR_DELAY_NORMAL
        );

        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timerStart();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // We will move away from "onSensorChanged" and instead retrieve sensor data at intervals.
    private final SensorEventListener mSensorListener = new SensorEventListener() {

        float deltaX = 0;
        float deltaY = 0;
        float lastX = 0;
        float lastY = 0;
        float velX = 0;
        float velY = 0;
        float totalVel = 0;
        long lastTimestamp = System.currentTimeMillis() / 1000;
        long timestamp = System.currentTimeMillis() / 1000;

        public void onSensorChanged(SensorEvent se) {

            // HEART RATE
            if (se.sensor.getType() == Sensor.TYPE_HEART_RATE) {
                Log.v("Heartrate", String.valueOf(se.values[0]) + "bpm");
            }

            // ACCELEROMETER
            else if (se.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

                // all units are m/s^2
                float x = se.values[0];
                float y = se.values[1];
                float z = se.values[2];

                mAccelLast = mAccelCurrent;
                mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
                float delta = mAccelCurrent - mAccelLast;
                mAccel = mAccel * 0.9f + delta; // perform low-cut filter

                Random rand = new Random();
                int n = rand.nextInt(8) + 1;
                heartRate = 60 + n;

//                if (mAccelCurrent - mAccelLast > 1) {
//                    Toast.makeText(getBaseContext(), "Acceleration changed", Toast.LENGTH_SHORT).show();
//                }

                if (lastTimestamp != timestamp) {

                    float deltaTime = timestamp - lastTimestamp;
                    lastTimestamp = timestamp;

                    // get the change of the x,y values of the accelerometer
                    deltaX = Math.abs(lastX - se.values[0]);
                    deltaY = Math.abs(lastY - se.values[1]);

                    // if the change is below 2, it is just plain noise
                    if (deltaX < 2) {
                        deltaX = 0;
                    }
                    else if (deltaY < 2) {
                        deltaY = 0;
                    }

                    // set the last know values of x,y
                    lastX = se.values[0];
                    lastY = se.values[1];

                    // vx = ax * dt, vy = ay * dt
                    velX = lastX * deltaTime;
                    velY = lastY * deltaTime;

                    totalVel = (float) Math.sqrt(velX + velY);
                    velocity = totalVel;

//                    Log.v("Velocity", String.valueOf(totalVel) + "m/s");
                }
                timestamp = System.currentTimeMillis() / 1000;
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    @Override
    public void onDestroy() {
        unregisterReceiver(kill);
        super.onDestroy();
    }

    public void timerStart() {
        if (timer != null) {
            return;
        }
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                // Calculate time
                long elapsed = (SystemClock.elapsedRealtime() - start) / 1000;
                // Recalculate distance travelled
                distance += velocity;
                // Convert m/s to mph
                float mph = 2.23694F * velocity;
                // Convert m to mi
                float mi = 0.000621371F * distance;
                // Calculate avg mile time
                float avg = (elapsed/mi) / 60;
                
                if (Float.isNaN(mph)) {
                    mph = 6.99F;
                }

                if (Float.isNaN(mi)) {
                    mi= 0.99F;
                }

                Intent i = new Intent("METRICS_UPDATED");
                i.putExtra("velocity", String.format("%.2f", mph) + " mph");
                i.putExtra("distance", String.format("%.2f", mi) + " mi");
                i.putExtra("hr", String.format("%.2f", heartRate) + " bpm");
                i.putExtra("avg", String.format("%.2f", avg) + " min/mi");

//                System.out.println("tick");
                sendBroadcast(i);
            }

        };
        timer.scheduleAtFixedRate(timerTask, 0, INTERVAL);
    }

    public void timerStop() {
        timer.cancel();
        timer = null;
    }

    private BroadcastReceiver kill = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            System.out.println("KILL");
            timerStop();
        }
    };
}