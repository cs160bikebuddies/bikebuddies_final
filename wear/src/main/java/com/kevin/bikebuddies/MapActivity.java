package com.kevin.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

public class MapActivity extends Activity {

    private ImageButton map;
    private String distance;
    private String time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = this.getIntent();
        time = intent.getStringExtra("time");
        distance = intent.getStringExtra("distance");

        ImageButton map = (ImageButton) findViewById(R.id.imageButton);
        map.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
                Intent myIntent = new Intent(MapActivity.this, FinishActivity.class);

                myIntent.putExtra("distance", distance);
                myIntent.putExtra("time", time);
                startActivity(myIntent);
            }
        });
    }
}
