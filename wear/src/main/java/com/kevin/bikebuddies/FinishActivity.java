package com.kevin.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

public class FinishActivity extends Activity {

    private static final String END_RIDE = "/end_ride";
    private ImageButton end;
    private GoogleApiClient mApiClient;

    private String time;
    private String distance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        Intent intent = this.getIntent();
        time = intent.getStringExtra("time");
        distance = intent.getStringExtra("distance");

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);
        layout.setBackground(getResources().getDrawable(R.drawable.empty_background));

        TextView view1 = (TextView) findViewById(R.id.textView4);
        TextView view2 = (TextView) findViewById(R.id.textView3);
        view1.setText("Time: " + time);
        view2.setText("Distance: " + distance);

        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        Button button = (Button) findViewById(R.id.button);
        button.setBackgroundColor(0x80000080);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(FinishActivity.this, MainActivity.class);
                startActivity(myIntent);

                Log.v("WatchSender", "HELLO");

                mApiClient.connect();
                sendMessage(END_RIDE);

                Intent i = new Intent("KILL");
                sendBroadcast(i);
            }
        });
    }

    private void sendMessage( final String path) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for (Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, null ).await();
                }
            }
        }).start();
    }
}
