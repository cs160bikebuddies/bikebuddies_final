package com.kevin.bikebuddies;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import org.w3c.dom.Text;

public class MetricsActivity1 extends Activity {

    private static final String METRICS_UPDATE = "/update";
    private GoogleApiClient mApiClient;

    private TextView view1;
    private TextView view2;
    private TextView view3;

    private String velocity = "0.00 mph";
    private String distance = "0.00 mi";
    private String hr = "0.00 bpm";

    private long time = 0;
    private String timestring;

    private int contxt = 0;
    private String syn;
    Chronometer timeElapsed;
    Intent i;

    int ride_id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metrics1);

        ride_id += 1;

        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        i = new Intent(this, BiometricsService.class);
        startService(i);

        registerReceiver(uiUpdated, new IntentFilter("METRICS_UPDATED"));

        timeElapsed  = (Chronometer) findViewById(R.id.chronometer);
        timeElapsed.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String hh = h < 10 ? "0" + h : h + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                timestring = hh + ":" + mm + ":" + ss;
                cArg.setText(timestring);
            }
        });
        timeElapsed.setBase(SystemClock.elapsedRealtime());
        timeElapsed.start();

        view1 = (TextView) findViewById(R.id.textView);
        view3 = (TextView) findViewById(R.id.textView2);
        view2 = (TextView) findViewById(R.id.textView3);


        RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);
        layout.setBackground(getResources().getDrawable(R.drawable.empty_background));
        layout.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
                Intent myIntent = new Intent(MetricsActivity1.this, MapActivity.class);
                myIntent.putExtra("distance", distance);
                myIntent.putExtra("time", timestring);
                startActivity(myIntent);
                kill();
            }
        });
    }

    private BroadcastReceiver uiUpdated= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            velocity = intent.getStringExtra("velocity");
            distance = intent.getStringExtra("distance");
            hr = intent.getStringExtra("hr");

            view1.setText(velocity);
            view2.setText(distance);
            view3.setText(hr);

            mApiClient.connect();
            sendMessage(METRICS_UPDATE, timestring + "," + velocity + "," + distance + "," + hr);
        }
    };

    @Override
    protected void onDestroy() {
//        unregisterReceiver(uiUpdated);
//        timeElapsed.stop();
//        mApiClient.disconnect();
//
//        stopService(i);
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(uiUpdated);
        timeElapsed.stop();
        mApiClient.disconnect();

//        stopService(i);
        super.onStop();
    }

    private void sendMessage( final String path, final String text ) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, text.getBytes() ).await();
                }
            }
        }).start();
    }

    private void kill() {
        this.finish();
    }
}