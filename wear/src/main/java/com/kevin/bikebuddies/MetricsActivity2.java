package com.kevin.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

public class MetricsActivity2 extends Activity {

    private ImageButton metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metrics2);

        ImageButton metrics = (ImageButton) findViewById(R.id.imageButton);
        metrics.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
                Intent myIntent = new Intent(MetricsActivity2.this, MetricsActivity3.class);
                startActivity(myIntent);
            }
        });
    }
}
