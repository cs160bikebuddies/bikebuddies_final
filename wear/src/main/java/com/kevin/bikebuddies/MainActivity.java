package com.kevin.bikebuddies;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

public class MainActivity extends WearableActivity {

//    private BoxInsetLayout mContainerView;
//    private ImageButton home;

    private static final String START_RIDE_ACTIVITY = "/start_ride_service";
    private GoogleApiClient mApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.layout);
        layout.setBackground(getResources().getDrawable(R.drawable.start_watch));

//        mContainerView = (BoxInsetLayout) findViewById(R.id.container);
//        ImageButton home = (ImageButton)findViewById(R.id.imageButton);

        layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, MetricsActivity1.class);
                startActivity(myIntent);

                mApiClient.connect();
                sendMessage(START_RIDE_ACTIVITY);
            }
        });
    }

    private void sendMessage( final String path) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, null ).await();
                }
            }
        }).start();
    }
}
